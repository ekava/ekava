package com.clamor1s.ekava.views.login;

import com.clamor1s.ekava.dto.request.LoginUserRequest;
import com.clamor1s.ekava.services.ClientService;
import com.clamor1s.ekava.views.MainLayout;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import lombok.extern.slf4j.Slf4j;

@PageTitle("Login")
@Route(value = "login", layout = MainLayout.class)
@AnonymousAllowed
@Uses(Icon.class)
@Slf4j
public class LoginView extends Composite<VerticalLayout> {

  public LoginView(ClientService clientService) {
    if (UI.getCurrent().getSession().getAttribute("access_token") != null
        && clientService.validateToken(UI.getCurrent().getSession().getAttribute("access_token").toString()) != null) {
      UI.getCurrent().navigateToClient("");
    }
    VerticalLayout layoutColumn2 = new VerticalLayout();

    EmailField emailField = new EmailField();
    PasswordField passwordField = new PasswordField();
    Button buttonPrimary = new Button();
    getContent().setWidth("100%");
    getContent().getStyle().set("flex-grow", "1");
    layoutColumn2.setWidthFull();
    getContent().setFlexGrow(1.0, layoutColumn2);
    layoutColumn2.setPadding(false);
    layoutColumn2.setWidth("100%");
    layoutColumn2.getStyle().set("flex-grow", "1");

    emailField.setLabel("Email");
    layoutColumn2.setAlignSelf(FlexComponent.Alignment.CENTER, emailField);
    emailField.setWidth("min-content");
    passwordField.setLabel("Password");
    layoutColumn2.setAlignSelf(FlexComponent.Alignment.CENTER, passwordField);
    passwordField.setWidth("min-content");
    buttonPrimary.setText("Login");
    layoutColumn2.setAlignSelf(FlexComponent.Alignment.CENTER, buttonPrimary);
    buttonPrimary.setWidth("90px");
    buttonPrimary.setMinWidth("100px");
    buttonPrimary.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

    buttonPrimary.addClickListener(event -> {
      boolean success = clientService.login(
          UI.getCurrent().getSession(),
          new LoginUserRequest(emailField.getValue(), passwordField.getValue())
      );
      if (success) {
        UI.getCurrent().getPage().reload();
        Notification.show("Success!");
        log.info(UI.getCurrent().getSession().getAttribute("access_token").toString());
      } else {
        Notification.show("Can't login :(");
      }
    });

    getContent().add(layoutColumn2);
    layoutColumn2.add(emailField);
    layoutColumn2.add(passwordField);
    layoutColumn2.add(buttonPrimary);
  }
}
