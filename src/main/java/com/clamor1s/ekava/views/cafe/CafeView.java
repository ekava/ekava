package com.clamor1s.ekava.views.cafe;

import com.clamor1s.ekava.dto.request.CafeRequestDto;
import com.clamor1s.ekava.dto.request.ItemRequestDto;
import com.clamor1s.ekava.dto.response.CafeResponseDto;
import com.clamor1s.ekava.dto.response.ItemResponseDto;
import com.clamor1s.ekava.services.CafeService;
import com.clamor1s.ekava.services.ClientService;
import com.clamor1s.ekava.services.ItemService;
import com.clamor1s.ekava.views.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.FlexComponent.JustifyContentMode;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.TabSheet;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.lumo.LumoUtility.Gap;
import jakarta.annotation.security.RolesAllowed;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;

@PageTitle("Cafe")
@Route(value = "cafe", layout = MainLayout.class)
@RolesAllowed("ADMIN")
@Uses(Icon.class)
@Slf4j
public class CafeView extends Composite<VerticalLayout> {

  private CafeService cafeService;
  private ItemService itemService;
  private ClientService clientService;

  public CafeView(CafeService cafeService, ItemService itemService, ClientService clientService) {
    this.cafeService = cafeService;
    this.itemService = itemService;
    this.clientService = clientService;

    if (UI.getCurrent().getSession().getAttribute("access_token") == null
        || clientService.validateToken(UI.getCurrent().getSession().getAttribute("access_token").toString()) == null) {
      UI.getCurrent().navigateToClient("login");
      return;
    }
    var client = clientService.getClient(UI.getCurrent().getSession().getAttribute("access_token").toString());
    if (client == null) {
      UI.getCurrent().getSession().setAttribute("access_token", null);
      UI.getCurrent().navigateToClient("login");
      return;
    }

    var roles = clientService.getRolesFromToken(UI.getCurrent().getSession().getAttribute("access_token").toString());

    HorizontalLayout layoutRow = new HorizontalLayout();
    Button logoutButton = new Button();
    VerticalLayout layoutColumn2 = new VerticalLayout();
    TabSheet tabSheet = new TabSheet();
    Grid<CafeResponseDto> cafeGrid = new Grid<>(CafeResponseDto.class, false);
    Grid<ItemResponseDto> itemGrid = new Grid<>(ItemResponseDto.class, false);
    cafeGrid.addColumn(CafeResponseDto::id).setHeader("Id").setWidth("250px");
    cafeGrid.addColumn(CafeResponseDto::name).setHeader("Name");
    cafeGrid.addColumn(CafeResponseDto::city).setHeader("City");
    cafeGrid.addColumn(CafeResponseDto::address).setHeader("Address");

    itemGrid.addColumn(ItemResponseDto::id).setHeader("Id").setWidth("250px");
    itemGrid.addColumn(ItemResponseDto::name).setHeader("Name");
    itemGrid.addColumn(ItemResponseDto::description).setHeader("Description");
    itemGrid.addColumn(ItemResponseDto::buyPrice).setHeader("Buy Price");
    Icon cafeIcon = new Icon();
    TextField searchCafeField = new TextField();
    Paragraph foundCafe = new Paragraph();
    foundCafe.setText("");
    foundCafe.setWidth("100%");
    foundCafe.getStyle().set("font-size", "var(--lumo-font-size-xl)");
    Paragraph foundItem = new Paragraph();
    foundItem.setText("");
    foundItem.setWidth("100%");
    foundItem.getStyle().set("font-size", "var(--lumo-font-size-xl)");
    TextField cafeNameField = new TextField();
    TextField cafeCityField = new TextField();
    TextField cafeAddressField = new TextField();
    Button cafeCreateButton = new Button();
    Icon itemIcon = new Icon();
    TextField itemSearchField = new TextField();
    TextField itemNameField = new TextField();
    itemNameField.setRequired(true);
    TextArea itemDescriptionField = new TextArea();
    NumberField itemPriceField = new NumberField();
    itemPriceField.setRequired(true);
    Button itemCreateButton = new Button();
    getContent().setWidth("100%");
    getContent().getStyle().set("flex-grow", "1");
    layoutRow.addClassName(Gap.MEDIUM);
    layoutRow.setWidth("100%");
    layoutRow.setHeight("min-content");
    layoutRow.setAlignItems(Alignment.START);
    layoutRow.setJustifyContentMode(JustifyContentMode.END);
    logoutButton.setText("Log Out");
    logoutButton.setWidth("min-content");
    layoutColumn2.setWidth("100%");
    layoutColumn2.getStyle().set("flex-grow", "1");
    tabSheet.setWidth("100%");

    cafeGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
    cafeGrid.setWidth("100%");
    cafeGrid.getStyle().set("flex-grow", "0");

    itemGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
    itemGrid.setWidth("100%");
    itemGrid.getStyle().set("flex-grow", "0");
    if (roles.contains("owner")) {
      setGridSampleDataCafe(cafeGrid);
    }
    if (roles.contains("owner") || roles.contains("worker")) {
      setGridSampleDataItem(itemGrid);
    }
    cafeIcon.getElement().setAttribute("icon", "lumo:user");
    searchCafeField.setLabel("Search Cafe");
    searchCafeField.setWidth("min-content");
    cafeNameField.setLabel("Name");
    cafeNameField.setRequired(true);
    cafeNameField.setWidth("min-content");
    cafeCityField.setLabel("City");
    cafeCityField.setRequired(true);
    cafeCityField.setWidth("min-content");
    cafeAddressField.setLabel("Address");
    cafeAddressField.setWidth("min-content");
    cafeAddressField.setRequired(true);
    cafeCreateButton.setText("Create Cafe");
    cafeCreateButton.setWidth("min-content");
    cafeCreateButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    itemIcon.getElement().setAttribute("icon", "lumo:user");
    itemSearchField.setLabel("Search Item");
    itemSearchField.setWidth("min-content");
    itemNameField.setLabel("Name");
    itemNameField.setWidth("min-content");
    itemDescriptionField.setLabel("Description");
    itemDescriptionField.setWidth("100%");
    itemPriceField.setLabel("Price");
    itemPriceField.setWidth("min-content");
    itemCreateButton.setText("Create Item");
    itemCreateButton.setWidth("min-content");
    itemCreateButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    getContent().add(layoutRow);
    layoutRow.add(logoutButton);
    searchCafeField.addKeyPressListener(Key.ENTER, event -> foundCafe.setText(
        cafeService.getCafeById(UI.getCurrent().getSession(), searchCafeField.getValue()) == null ? "" :
            cafeService.getCafeById(UI.getCurrent().getSession(), searchCafeField.getValue()).toString()));
    itemSearchField.addKeyPressListener(Key.ENTER, event -> {
      var item = itemService.getItemById(UI.getCurrent().getSession(), itemSearchField.getValue());
      foundItem.setText(item == null ? "" : item.toString());
    });
    cafeCreateButton.addClickListener(event -> {
      if (cafeNameField.isEmpty() || cafeCityField.isEmpty() || cafeAddressField.isEmpty()) {
        return;
      }
      cafeService.createCafe(UI.getCurrent().getSession(),
          new CafeRequestDto(cafeNameField.getValue(), cafeCityField.getValue(), cafeAddressField.getValue()));
      UI.getCurrent().getPage().reload();
    });
    itemCreateButton.addClickListener(event -> {
      if (itemNameField.isEmpty() || itemPriceField.isEmpty()) {
        return;
      }
      itemService.createItem(UI.getCurrent().getSession(),
          new ItemRequestDto(itemNameField.getValue(), itemDescriptionField.isEmpty() ? null : itemDescriptionField.getValue(),
              BigDecimal.valueOf(itemPriceField.getValue())));
      UI.getCurrent().getPage().reload();
    });
    logoutButton.addClickListener(event -> {
      UI.getCurrent().getSession().setAttribute("access_token", null);
      UI.getCurrent().navigateToClient("login");
      UI.getCurrent().getPage().reload();
    });
    setTabSheetSampleCafeData(tabSheet, cafeGrid, cafeIcon, searchCafeField, foundCafe, cafeNameField, cafeCityField, cafeAddressField,
        cafeCreateButton);
    setTabSheetSampleItemData(tabSheet, itemGrid, itemIcon, itemSearchField, foundItem, itemNameField, itemDescriptionField, itemPriceField,
        itemCreateButton);
    getContent().add(layoutColumn2);
    layoutColumn2.add(tabSheet);
  }

  private void setTabSheetSampleCafeData(TabSheet tabSheet, Grid<?> cafeGrid, Component... components) {
    var roles = clientService.getRolesFromToken(UI.getCurrent().getSession().getAttribute("access_token").toString());
    var verticalLayout = new VerticalLayout();
    verticalLayout.add(new Text("This is the Cafe tab content"));
    if (roles.contains("owner")) {
      verticalLayout.add(cafeGrid);
    }
    if (!roles.contains("owner")) {
      verticalLayout.add(Arrays.stream(components).limit(3).collect(Collectors.toList()));
    } else {
      verticalLayout.add(components);
    }
    tabSheet.add("Cafe", verticalLayout);
  }

  private void setTabSheetSampleItemData(TabSheet tabSheet, Grid<?> itemGrid, Component... components) {
    var roles = clientService.getRolesFromToken(UI.getCurrent().getSession().getAttribute("access_token").toString());
    var verticalLayout = new VerticalLayout();
    verticalLayout.add(new Text("This is the Item tab content"));
    if (roles.contains("owner") || roles.contains("worker")) {
      verticalLayout.add(itemGrid);
    }
    if (!roles.contains("owner")) {
      verticalLayout.add(Arrays.stream(components).limit(3).collect(Collectors.toList()));
    } else {
      verticalLayout.add(components);
    }
    tabSheet.add("Item", verticalLayout);
  }

  private void setGridSampleDataCafe(Grid grid) {
    var data = cafeService.getAllCafes(UI.getCurrent().getSession());
    grid.setItems(data);
  }

  private void setGridSampleDataItem(Grid grid) {
    var data = itemService.getAllItems(UI.getCurrent().getSession());
    grid.setItems(data);
  }
}
