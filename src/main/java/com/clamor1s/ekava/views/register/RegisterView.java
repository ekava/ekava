package com.clamor1s.ekava.views.register;

import com.clamor1s.ekava.dto.request.CreateUserRequest;
import com.clamor1s.ekava.services.ClientService;
import com.clamor1s.ekava.views.MainLayout;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@PageTitle("Register")
@Route(value = "register", layout = MainLayout.class)
@AnonymousAllowed
@Uses(Icon.class)
@Slf4j
public class RegisterView extends Composite<VerticalLayout> {
  @Autowired
  private ClientService clientService;

  public RegisterView() {
    VerticalLayout layoutColumn2 = new VerticalLayout();
    TextField nameTextField = new TextField();
    TextField surnameTextField = new TextField();
    EmailField emailField = new EmailField();
    PasswordField passwordField = new PasswordField();
    Button registerButton = new Button();
    getContent().setWidth("100%");
    getContent().getStyle().set("flex-grow", "1");
    layoutColumn2.setWidthFull();
    getContent().setFlexGrow(1.0, layoutColumn2);
    layoutColumn2.setPadding(false);
    layoutColumn2.setWidth("100%");
    layoutColumn2.getStyle().set("flex-grow", "1");
    nameTextField.setLabel("Name");
    layoutColumn2.setAlignSelf(FlexComponent.Alignment.CENTER, nameTextField);
    nameTextField.setWidth("192px");
    surnameTextField.setLabel("Surname");
    layoutColumn2.setAlignSelf(FlexComponent.Alignment.CENTER, surnameTextField);
    surnameTextField.setWidth("min-content");
    emailField.setLabel("Email");
    layoutColumn2.setAlignSelf(FlexComponent.Alignment.CENTER, emailField);
    emailField.setWidth("min-content");
    passwordField.setLabel("Password");
    layoutColumn2.setAlignSelf(FlexComponent.Alignment.CENTER, passwordField);
    passwordField.setWidth("min-content");
    registerButton.setText("Register");
    layoutColumn2.setAlignSelf(FlexComponent.Alignment.CENTER, registerButton);
    registerButton.setWidth("90px");
    registerButton.setMinWidth("100px");

    registerButton.addClickListener(
        event -> {
          boolean success = clientService.registerUser(
              new CreateUserRequest(
                  nameTextField.getValue(),
                  surnameTextField.getValue(),
                  emailField.getValue(),
                  passwordField.getValue()
              )
          );
          if (success) {
            UI.getCurrent().navigateToClient("login");
            UI.getCurrent().getPage().reload();
            Notification.show("Success!");
          } else {
            Notification.show("Can't register the user :(");
          }
        }
    );

    registerButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    getContent().add(layoutColumn2);
    layoutColumn2.add(nameTextField);
    layoutColumn2.add(surnameTextField);
    layoutColumn2.add(emailField);
    layoutColumn2.add(passwordField);
    layoutColumn2.add(registerButton);
  }
}
