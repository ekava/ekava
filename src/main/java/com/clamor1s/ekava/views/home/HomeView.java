package com.clamor1s.ekava.views.home;

import com.clamor1s.ekava.services.ClientService;
import com.clamor1s.ekava.views.MainLayout;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import com.vaadin.flow.theme.lumo.LumoUtility;

@PageTitle("Home")
@Route(value = "", layout = MainLayout.class)
@AnonymousAllowed
@Uses(Icon.class)
public class HomeView extends Composite<VerticalLayout> {

  public HomeView(ClientService clientService) {
    if (UI.getCurrent().getSession().getAttribute("access_token") == null
        || clientService.validateToken(UI.getCurrent().getSession().getAttribute("access_token").toString()) == null) {
      UI.getCurrent().navigateToClient("login");
      return;
    }
    var client = clientService.getClient(UI.getCurrent().getSession().getAttribute("access_token").toString());
    if (client == null) {
      UI.getCurrent().getSession().setAttribute("access_token", null);
      UI.getCurrent().navigateToClient("login");
      return;
    }
    HorizontalLayout layoutRow = new HorizontalLayout();
    Paragraph idText = new Paragraph();
    Paragraph nameText = new Paragraph();
    Paragraph surnameText = new Paragraph();
    Paragraph emailText = new Paragraph();
    getContent().setWidth("100%");
    getContent().getStyle().set("flex-grow", "1");
    layoutRow.addClassName(LumoUtility.Gap.MEDIUM);
    layoutRow.setWidth("100%");
    layoutRow.setHeight("min-content");
    layoutRow.setAlignItems(FlexComponent.Alignment.START);
    layoutRow.setJustifyContentMode(FlexComponent.JustifyContentMode.END);
    Button logoutButton = new Button();
    logoutButton.setText("Log Out");
    logoutButton.setWidth("min-content");
    nameText.setText("Name: " + client.name());
    nameText.setWidth("100%");
    nameText.getStyle().set("font-size", "var(--lumo-font-size-xl)");
    surnameText.setText("Surname: " + client.surname());
    surnameText.setWidth("100%");
    surnameText.getStyle().set("font-size", "var(--lumo-font-size-xl)");
    emailText.setText("Email: " + client.email());
    emailText.setWidth("100%");
    emailText.getStyle().set("font-size", "var(--lumo-font-size-xl)");
    idText.setText("Id: " + client.id());
    idText.setWidth("100%");
    idText.getStyle().set("font-size", "var(--lumo-font-size-xl)");

    logoutButton.addClickListener(event -> {
      UI.getCurrent().getSession().setAttribute("access_token", null);
      UI.getCurrent().navigateToClient("login");
      UI.getCurrent().getPage().reload();
    });

    layoutRow.add(logoutButton);
    getContent().add(layoutRow);
    getContent().add(idText);
    getContent().add(nameText);
    getContent().add(surnameText);
    getContent().add(emailText);
  }
}
