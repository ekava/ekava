package com.clamor1s.ekava.views.transactions;

import com.clamor1s.ekava.dto.request.TransactionRequestDto;
import com.clamor1s.ekava.dto.response.TransactionResponseDto;
import com.clamor1s.ekava.services.ClientService;
import com.clamor1s.ekava.services.StatisticService;
import com.clamor1s.ekava.services.TransactionService;
import com.clamor1s.ekava.views.MainLayout;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.FlexComponent.JustifyContentMode;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.lumo.LumoUtility.Gap;
import jakarta.annotation.security.RolesAllowed;

import java.math.BigDecimal;
import java.util.UUID;

@PageTitle("Transactions")
@Route(value = "transactions", layout = MainLayout.class)
@RolesAllowed("USER")
@Uses(Icon.class)
public class TransactionsView extends Composite<VerticalLayout> {

    private TransactionService transactionService;

    public TransactionsView(TransactionService transactionService, ClientService clientService, StatisticService statisticService) {
        this.transactionService = transactionService;

        if (UI.getCurrent().getSession().getAttribute("access_token") == null
                || clientService.validateToken(UI.getCurrent().getSession().getAttribute("access_token").toString()) == null) {
            UI.getCurrent().navigateToClient("login");
            return;
        }
        var client = clientService.getClient(UI.getCurrent().getSession().getAttribute("access_token").toString());
        if (client == null) {
            UI.getCurrent().getSession().setAttribute("access_token", null);
            UI.getCurrent().navigateToClient("login");
            return;
        }
        var roles = clientService.getRolesFromToken(UI.getCurrent().getSession().getAttribute("access_token").toString());

        HorizontalLayout layoutRow = new HorizontalLayout();
        Button logoutButton = new Button();
        VerticalLayout layoutColumn2 = new VerticalLayout();
        VerticalLayout layoutColumn3 = new VerticalLayout();
        TextField itemIdField = new TextField();
        NumberField priceField = new NumberField();
        HorizontalLayout layoutRow2 = new HorizontalLayout();
        Button buyButton = new Button();
        Button sellButton = new Button();
        Button fixButton = new Button();
        Grid<TransactionResponseDto> transactionGrid = new Grid<>(TransactionResponseDto.class);
        transactionGrid.addColumn(TransactionResponseDto::id).setHeader("Id").setWidth("250px");
        transactionGrid.addColumn(TransactionResponseDto::date).setHeader("Date");
        transactionGrid.addColumn(TransactionResponseDto::clientId).setHeader("Client Id");
        transactionGrid.addColumn(TransactionResponseDto::itemId).setHeader("Item Id");
        transactionGrid.addColumn(TransactionResponseDto::price).setHeader("Price");
        transactionGrid.addColumn(TransactionResponseDto::type).setHeader("Address");
        transactionGrid.addColumn(TransactionResponseDto::status).setHeader("Status");
        Paragraph profitText = new Paragraph();
        getContent().setWidth("100%");
        getContent().getStyle().set("flex-grow", "1");
        layoutRow.addClassName(Gap.MEDIUM);
        layoutRow.setWidth("100%");
        layoutRow.setHeight("min-content");
        layoutRow.setAlignItems(Alignment.START);
        layoutRow.setJustifyContentMode(JustifyContentMode.END);
        logoutButton.setText("Log Out");
        logoutButton.setWidth("min-content");
        layoutColumn2.setWidth("100%");
        layoutColumn2.getStyle().set("flex-grow", "1");
        layoutColumn3.setWidthFull();
        layoutColumn2.setFlexGrow(1.0, layoutColumn3);
        layoutColumn3.setWidth("100%");
        layoutColumn3.getStyle().set("flex-grow", "1");
        itemIdField.setLabel("Item Id");
        itemIdField.setRequired(true);
        itemIdField.setWidth("min-content");
        priceField.setLabel("Price");
        priceField.setRequired(true);
        priceField.setWidth("min-content");
        layoutRow2.setWidthFull();
        layoutColumn3.setFlexGrow(1.0, layoutRow2);
        layoutRow2.addClassName(Gap.MEDIUM);
        layoutRow2.setWidth("100%");
        layoutRow2.setHeight("50px");
        buyButton.setText("Buy");
        buyButton.setWidth("min-content");
        buyButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        sellButton.setText("Sell");
        sellButton.setWidth("min-content");
        fixButton.setText("Fix");
        fixButton.setWidth("min-content");
        fixButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        transactionGrid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER,
                GridVariant.LUMO_NO_ROW_BORDERS);
        transactionGrid.setWidth("100%");
        transactionGrid.getStyle().set("flex-grow", "0");
        setGridSampleData(transactionGrid);
        profitText.setText("Profit: 0.0");
        profitText.setWidth("100%");
        profitText.getStyle().set("font-size", "var(--lumo-font-size-xl)");

        buyButton.addClickListener(event -> {
            if (itemIdField.isEmpty() || priceField.isEmpty()) {
                return;
            }
            transactionService.buyProduct(UI.getCurrent().getSession(),
                    new TransactionRequestDto(UUID.fromString(itemIdField.getValue()), BigDecimal.valueOf(priceField.getValue())));
            UI.getCurrent().getPage().reload();
        });
        sellButton.addClickListener(event -> {
            if (itemIdField.isEmpty() || priceField.isEmpty()) {
                return;
            }
            transactionService.sellProduct(UI.getCurrent().getSession(),
                    new TransactionRequestDto(UUID.fromString(itemIdField.getValue()), BigDecimal.valueOf(priceField.getValue())));
            UI.getCurrent().getPage().reload();
        });
        fixButton.addClickListener(event -> {
            if (itemIdField.isEmpty() || priceField.isEmpty()) {
                return;
            }
            transactionService.fixProduct(UI.getCurrent().getSession(),
                    new TransactionRequestDto(UUID.fromString(itemIdField.getValue()), BigDecimal.valueOf(priceField.getValue())));
            UI.getCurrent().getPage().reload();
        });

        logoutButton.addClickListener(event -> {
            UI.getCurrent().getSession().setAttribute("access_token", null);
            UI.getCurrent().navigateToClient("login");
            UI.getCurrent().getPage().reload();
        });

        getContent().add(layoutRow);
        layoutRow.add(logoutButton);
        getContent().add(layoutColumn2);
        layoutColumn2.add(layoutColumn3);
        layoutColumn3.add(itemIdField);
        layoutColumn3.add(priceField);
        layoutColumn3.add(layoutRow2);
        layoutRow2.add(sellButton);
        if (roles.contains("worker") || roles.contains("owner")) {
            layoutRow2.add(buyButton);
            layoutRow2.add(fixButton);
        }
        layoutColumn3.add(transactionGrid);
        if (roles.contains("owner")) {
            profitText.setText(statisticService.getProfit(UI.getCurrent().getSession()).toString());
            layoutColumn3.add(profitText);
        }
    }

    private void setGridSampleData(Grid grid) {
        var data = transactionService.getAllTransactions(UI.getCurrent().getSession());
        grid.setItems(data);
    }
}
