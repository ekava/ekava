package com.clamor1s.ekava.views;

import com.clamor1s.ekava.services.ClientService;
import com.clamor1s.ekava.views.cafe.CafeView;
import com.clamor1s.ekava.views.home.HomeView;
import com.clamor1s.ekava.views.login.LoginView;
import com.clamor1s.ekava.views.register.RegisterView;
import com.clamor1s.ekava.views.transactions.TransactionsView;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.Footer;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Header;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.sidenav.SideNav;
import com.vaadin.flow.component.sidenav.SideNavItem;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.theme.lumo.LumoUtility;
import org.vaadin.lineawesome.LineAwesomeIcon;

/**
 * The main view is a top-level placeholder for other views.
 */
public class MainLayout extends AppLayout {

    private H2 viewTitle;
    private ClientService clientService;

//    private AuthenticatedUser authenticatedUser;
//    private AccessAnnotationChecker accessChecker;

    public MainLayout(ClientService clientService/*AuthenticatedUser authenticatedUser, AccessAnnotationChecker accessChecker*/) {
//        this.authenticatedUser = authenticatedUser;
//        this.accessChecker = accessChecker;
        this.clientService = clientService;

        setPrimarySection(Section.DRAWER);
        addDrawerContent();
        addHeaderContent();
    }

    private void addHeaderContent() {
        DrawerToggle toggle = new DrawerToggle();
        toggle.setAriaLabel("Menu toggle");

        viewTitle = new H2();
        viewTitle.addClassNames(LumoUtility.FontSize.LARGE, LumoUtility.Margin.NONE);

        addToNavbar(true, toggle, viewTitle);
    }

    private void addDrawerContent() {
        H1 appName = new H1("eKava");
        appName.addClassNames(LumoUtility.FontSize.LARGE, LumoUtility.Margin.NONE);
        Header header = new Header(appName);

        Scroller scroller = new Scroller(createNavigation());

        addToDrawer(header, scroller, createFooter());
    }

    private SideNav createNavigation() {
        SideNav nav = new SideNav();

        if (UI.getCurrent().getSession().getAttribute("access_token") != null
                && clientService.validateToken(UI.getCurrent().getSession().getAttribute("access_token").toString()) != null) {
            nav.addItem(new SideNavItem("Home", HomeView.class, LineAwesomeIcon.HOME_SOLID.create()));
        }
        if (UI.getCurrent().getSession().getAttribute("access_token") == null
                || clientService.validateToken(UI.getCurrent().getSession().getAttribute("access_token").toString()) == null) {
            nav.addItem(new SideNavItem("Register", RegisterView.class, LineAwesomeIcon.USER_PLUS_SOLID.create()));
        }

        if (UI.getCurrent().getSession().getAttribute("access_token") == null
                || clientService.validateToken(UI.getCurrent().getSession().getAttribute("access_token").toString()) == null) {
            nav.addItem(new SideNavItem("Login", LoginView.class, LineAwesomeIcon.USER_SOLID.create()));
        }
        if (UI.getCurrent().getSession().getAttribute("access_token") != null
                && clientService.validateToken(UI.getCurrent().getSession().getAttribute("access_token").toString()) != null) {
            var roles = clientService.getRolesFromToken(UI.getCurrent().getSession().getAttribute("access_token").toString());
            if (roles.contains("owner") || roles.contains("worker")) {
                nav.addItem(new SideNavItem("Cafe", CafeView.class, LineAwesomeIcon.HAMBURGER_SOLID.create()));
            }
        }
        if (UI.getCurrent().getSession().getAttribute("access_token") != null
                && clientService.validateToken(UI.getCurrent().getSession().getAttribute("access_token").toString()) != null) {
            nav.addItem(new SideNavItem("Transactions", TransactionsView.class, LineAwesomeIcon.MONEY_BILL_SOLID.create()));
        }

        return nav;
    }

    private Footer createFooter() {
        Footer layout = new Footer();

//        Optional<User> maybeUser = authenticatedUser.get();
//        if (maybeUser.isPresent()) {
//            User user = maybeUser.get();
//
//            Avatar avatar = new Avatar(user.getName());
//            StreamResource resource = new StreamResource("profile-pic",
//                    () -> new ByteArrayInputStream(user.getProfilePicture()));
//            avatar.setImageResource(resource);
//            avatar.setThemeName("xsmall");
//            avatar.getElement().setAttribute("tabindex", "-1");
//
//            MenuBar userMenu = new MenuBar();
//            userMenu.setThemeName("tertiary-inline contrast");
//
//            MenuItem userName = userMenu.addItem("");
//            Div div = new Div();
//            div.add(avatar);
//            div.add(user.getName());
//            div.add(new Icon("lumo", "dropdown"));
//            div.getElement().getStyle().set("display", "flex");
//            div.getElement().getStyle().set("align-items", "center");
//            div.getElement().getStyle().set("gap", "var(--lumo-space-s)");
//            userName.add(div);
//            userName.getSubMenu().addItem("Sign out", e -> {
//                authenticatedUser.logout();
//            });
//
//            layout.add(userMenu);
//        } else {
//            Anchor loginLink = new Anchor("login", "Sign in");
//            layout.add(loginLink);
//        }

        return layout;
    }

    @Override
    protected void afterNavigation() {
        super.afterNavigation();
        viewTitle.setText(getCurrentPageTitle());
    }

    private String getCurrentPageTitle() {
        PageTitle title = getContent().getClass().getAnnotation(PageTitle.class);
        return title == null ? "" : title.value();
    }
}
