package com.clamor1s.ekava.feignclient;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

import com.clamor1s.ekava.dto.request.ItemRequestDto;
import com.clamor1s.ekava.dto.response.ItemResponseDto;
import java.util.List;
import java.util.UUID;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value = "item-service", url = "${client.cafe-service.host}")
public interface ItemFeign {
  @GetMapping(value = "/auth/items")
  List<ItemResponseDto> getItems(@RequestHeader(AUTHORIZATION) String authHeader);

  @GetMapping(value = "/auth/items/{id}")
  ItemResponseDto getItemById(@RequestHeader(AUTHORIZATION) String authHeader,
                              @PathVariable UUID id);

  @PostMapping(value = "/auth/items")
  void createItem(@RequestHeader(AUTHORIZATION) String authHeader,
                  @RequestBody ItemRequestDto itemRequestDto);
}
