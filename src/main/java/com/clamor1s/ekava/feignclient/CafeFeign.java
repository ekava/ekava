package com.clamor1s.ekava.feignclient;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

import com.clamor1s.ekava.dto.request.CafeRequestDto;
import com.clamor1s.ekava.dto.response.CafeResponseDto;
import java.util.List;
import java.util.UUID;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value = "cafe-service", url = "${client.cafe-service.host}")
public interface CafeFeign {
  @GetMapping(value = "/auth/cafes")
  List<CafeResponseDto> getCafes(@RequestHeader(AUTHORIZATION) String authHeader);

  @GetMapping(value = "/auth/cafes/{id}")
  CafeResponseDto getCafeById(@RequestHeader(AUTHORIZATION) String authHeader,
                              @PathVariable UUID id);

  @PostMapping(value = "/auth/cafes")
  void createCafe(@RequestHeader(AUTHORIZATION) String authHeader,
                  @RequestBody CafeRequestDto cafeRequestDto);
}
