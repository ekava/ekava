package com.clamor1s.ekava.feignclient;

import com.clamor1s.ekava.dto.request.TransactionRequestDto;
import com.clamor1s.ekava.dto.response.TransactionResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@FeignClient(value = "transaction-service", url = "${client.transaction-service.host}")
public interface TransactionFeign {
    @GetMapping(value = "/auth/transactions")
    List<TransactionResponseDto> getAllTransactions(@RequestHeader(AUTHORIZATION) String authHeader);

    @PostMapping(value = "/auth/transactions/buy")
    void buyProduct(@RequestHeader(AUTHORIZATION) String authHeader,
                    @RequestBody TransactionRequestDto transactionRequestDto);

    @PostMapping(value = "/auth/transactions/sell")
    void sellProduct(@RequestHeader(AUTHORIZATION) String authHeader,
                    @RequestBody TransactionRequestDto transactionRequestDto);

    @PostMapping(value = "/auth/transactions/fix")
    void fixProduct(@RequestHeader(AUTHORIZATION) String authHeader,
                    @RequestBody TransactionRequestDto transactionRequestDto);
}
