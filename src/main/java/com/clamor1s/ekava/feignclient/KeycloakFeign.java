package com.clamor1s.ekava.feignclient;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

import com.clamor1s.ekava.dto.response.KeycloakTokenResponse;
import com.clamor1s.ekava.dto.response.KeycloakUserRoleResponse;
import java.util.List;
import java.util.Map;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value = "keycloak-service", url = "${client.keycloak-service.host}")
public interface KeycloakFeign {
  @PostMapping(value = "/protocol/openid-connect/token", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  KeycloakTokenResponse getClientToken(@RequestBody Map<String, ?> formData);

  @GetMapping("/roles")
  List<KeycloakUserRoleResponse> getRoles(@RequestHeader(AUTHORIZATION) String authHeader);
}
