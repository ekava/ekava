package com.clamor1s.ekava.feignclient;


import static org.springframework.http.HttpHeaders.AUTHORIZATION;

import com.clamor1s.ekava.dto.request.CreateUserRequest;
import com.clamor1s.ekava.dto.response.ClientResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value = "client-service", url = "${client.client-service.host}")
public interface ClientFeign {
  @PostMapping("/clients")
  ResponseEntity<?> createClient(@RequestBody CreateUserRequest userRequest);

  @GetMapping(value = "/auth/clients")
  ClientResponse getClientByToken(@RequestHeader(AUTHORIZATION) String bearerToken);
}
