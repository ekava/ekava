package com.clamor1s.ekava.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import java.math.BigDecimal;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@FeignClient(value = "statistic-service", url = "${client.statistic-service.host}")
public interface StatisticFeign {
    @GetMapping(value = "/auth/statistics/profit")
    BigDecimal getProfit(@RequestHeader(AUTHORIZATION) String authHeader);
}
