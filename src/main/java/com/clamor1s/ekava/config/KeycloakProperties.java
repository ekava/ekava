package com.clamor1s.ekava.config;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Slf4j
@Configuration
@ConfigurationProperties(prefix = "client.keycloak-service")
public class KeycloakProperties {
  private String clientSecret;
  private String publicKey;
}
