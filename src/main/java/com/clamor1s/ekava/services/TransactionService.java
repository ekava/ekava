package com.clamor1s.ekava.services;

import com.clamor1s.ekava.dto.request.TransactionRequestDto;
import com.clamor1s.ekava.dto.response.TransactionResponseDto;
import com.vaadin.flow.server.VaadinSession;

import java.util.List;

public interface TransactionService {
    List<TransactionResponseDto> getAllTransactions(VaadinSession session);

    void buyProduct(VaadinSession session, TransactionRequestDto requestDto);

    void sellProduct(VaadinSession session, TransactionRequestDto requestDto);

    void fixProduct(VaadinSession session, TransactionRequestDto requestDto);
}
