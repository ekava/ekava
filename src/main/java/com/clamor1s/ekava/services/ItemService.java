package com.clamor1s.ekava.services;

import com.clamor1s.ekava.dto.request.CafeRequestDto;
import com.clamor1s.ekava.dto.request.ItemRequestDto;
import com.clamor1s.ekava.dto.response.CafeResponseDto;
import com.clamor1s.ekava.dto.response.ItemResponseDto;
import com.vaadin.flow.server.VaadinSession;
import java.util.List;

public interface ItemService {
  List<ItemResponseDto> getAllItems(VaadinSession session);

  ItemResponseDto getItemById(VaadinSession session, String id);

  void createItem(VaadinSession session, ItemRequestDto itemRequestDto);
}
