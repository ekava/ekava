package com.clamor1s.ekava.services.impl;

import com.clamor1s.ekava.feignclient.StatisticFeign;
import com.clamor1s.ekava.services.ClientService;
import com.clamor1s.ekava.services.StatisticService;
import com.vaadin.flow.server.VaadinSession;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
@Slf4j
public class StatisticServiceImpl implements StatisticService {
    private final ClientService clientService;
    private final StatisticFeign statisticFeign;

    @Override
    public BigDecimal getProfit(VaadinSession session) {
        try {
            var token = session.getAttribute("access_token").toString();
            if (token.isEmpty()) {
                return null;
            }
            var roles = clientService.getRolesFromToken(token);
            if (!roles.contains("owner")) {
                return null;
            }
            return statisticFeign.getProfit("Bearer " + session.getAttribute("access_token").toString());
        } catch (FeignException e) {
            log.error("Can't get profit :(");
            return null;
        }
    }
}
