package com.clamor1s.ekava.services.impl;

import com.clamor1s.ekava.dto.request.ItemRequestDto;
import com.clamor1s.ekava.dto.response.ItemResponseDto;
import com.clamor1s.ekava.feignclient.ItemFeign;
import com.clamor1s.ekava.services.ClientService;
import com.clamor1s.ekava.services.ItemService;
import com.vaadin.flow.server.VaadinSession;
import feign.FeignException;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ItemServiceImpl implements ItemService {

  private final ItemFeign itemFeign;
  private final ClientService clientService;

  @Override
  public List<ItemResponseDto> getAllItems(VaadinSession session) {
    try {
      var token = session.getAttribute("access_token").toString();
      if (token.isEmpty()) {
        return null;
      }
      var roles = clientService.getRolesFromToken(token);
      if (!roles.contains("owner") && !roles.contains("worker")) {
        return null;
      }
      return itemFeign.getItems("Bearer " + token);
    } catch (FeignException e) {
      return null;
    }
  }

  @Override
  public ItemResponseDto getItemById(VaadinSession session, String id) {
    try {
      var token = session.getAttribute("access_token").toString();
      if (token.isEmpty()) {
        return null;
      }
      var roles = clientService.getRolesFromToken(token);
      if (!roles.contains("owner") && !roles.contains("worker")) {
        return null;
      }
      return itemFeign.getItemById("Bearer " + token, UUID.fromString(id));
    } catch (FeignException | IllegalArgumentException e) {
      return null;
    }
  }

  @Override
  public void createItem(VaadinSession session, ItemRequestDto itemRequestDto) {
    try {
      String token = session.getAttribute("access_token").toString();
      if (token.isEmpty()) {
        session.setAttribute("access_token", null);
      }
      var roles = clientService.getRolesFromToken(token);
      if (!roles.contains("owner")) {
        return;
      }
      itemFeign.createItem("Bearer " + token, itemRequestDto);
    } catch (FeignException e) {
      log.error("Cant create cafe");
    }
  }
}
