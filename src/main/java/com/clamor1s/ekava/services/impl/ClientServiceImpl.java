package com.clamor1s.ekava.services.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.clamor1s.ekava.config.KeycloakProperties;
import com.clamor1s.ekava.dto.request.CreateUserRequest;
import com.clamor1s.ekava.dto.request.LoginUserRequest;
import com.clamor1s.ekava.dto.response.ClientResponse;
import com.clamor1s.ekava.feignclient.ClientFeign;
import com.clamor1s.ekava.feignclient.KeycloakFeign;
import com.clamor1s.ekava.services.ClientService;
import com.clamor1s.ekava.util.jwt.JwtValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.server.VaadinSession;
import feign.FeignException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ClientServiceImpl implements ClientService {
  private final ClientFeign clientFeign;
  private final KeycloakFeign keycloakFeign;
  private final KeycloakProperties keycloakProperties;
  private final JwtValidator jwtValidator;

  @Override
  public boolean registerUser(@NonNull CreateUserRequest userRequest) {
    try {
      clientFeign.createClient(userRequest);
    } catch (FeignException e) {
      log.warn("Can't create a user:(");
      return false;
    }
    return true;
  }

  @Override
  public boolean login(VaadinSession session, LoginUserRequest userRequest) {
    var formData = getFormRequest(userRequest.username(), userRequest.password());
    try {
      var response = keycloakFeign.getClientToken(formData);
      session.setAttribute("access_token", response.access_token());
    } catch (FeignException e) {
      log.warn("Can't Login :(");
      return false;
    }
    return true;
  }

  @Override
  public DecodedJWT validateToken(@Nullable String token) {
    if (token == null) {
      return null;
    }
    try {
      return jwtValidator.validate(token);
    } catch (JWTVerificationException | IllegalArgumentException | IllegalAccessException e) {
      return null;
    }
  }

  @Override
  public ClientResponse getClient(String bearerToken) {
    try {
      var roles = getRolesFromToken(bearerToken);
      if (!roles.contains("owner") && !roles.contains("worker") && !roles.contains("user")) {
        return null;
      }
      return clientFeign.getClientByToken("Bearer " + bearerToken);
    } catch (FeignException e) {
      return null;
    }
  }

  @Override
  public String getRolesFromToken(String bearerToken) {
    ObjectMapper objectMapper = new ObjectMapper();
    String realmAccess = new JWT().decodeJwt(bearerToken).getClaim("realm_access").toString();
    try {
      return objectMapper.readTree(realmAccess).findValue("roles").toString();
    } catch (IOException e) {
      log.error("Illegal token");
      return "";
    }
  }

  private Map<String, String> getFormRequest(String username, String password) {
    Map<String, String> form = new HashMap<>();
    form.put("grant_type", "password");
    form.put("client_id", "eKava");
    form.put("username", username);
    form.put("password", password);
    form.put("client_secret", keycloakProperties.getClientSecret());
    return form;
  }
}
