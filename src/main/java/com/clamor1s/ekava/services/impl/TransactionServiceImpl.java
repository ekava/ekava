package com.clamor1s.ekava.services.impl;

import com.clamor1s.ekava.dto.request.TransactionRequestDto;
import com.clamor1s.ekava.dto.response.TransactionResponseDto;
import com.clamor1s.ekava.feignclient.TransactionFeign;
import com.clamor1s.ekava.services.ClientService;
import com.clamor1s.ekava.services.TransactionService;
import com.vaadin.flow.server.VaadinSession;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransactionServiceImpl implements TransactionService {
    private final ClientService clientService;
    private final TransactionFeign transactionFeign;

    @Override
    public List<TransactionResponseDto> getAllTransactions(VaadinSession session) {
        try {
            var token = session.getAttribute("access_token").toString();
            if (token.isEmpty()) {
                return null;
            }
            var roles = clientService.getRolesFromToken(token);
            if (!roles.contains("owner") && !roles.contains("worker") && !roles.contains("user")) {
                return null;
            }
            return transactionFeign.getAllTransactions("Bearer " + token);
        } catch (FeignException e) {
            log.error("Can't get transactions");
            return null;
        }
    }

    @Override
    public void buyProduct(VaadinSession session, TransactionRequestDto requestDto) {
        try {
            var token = session.getAttribute("access_token").toString();
            if (token.isEmpty()) {
                return;
            }
            var roles = clientService.getRolesFromToken(token);
            if (!roles.contains("owner") && !roles.contains("worker")) {
                return;
            }
            transactionFeign.buyProduct("Bearer " + token, requestDto);
        } catch (FeignException e) {
            log.error("Can't buy product");
        }
    }

    @Override
    public void sellProduct(VaadinSession session, TransactionRequestDto requestDto) {
        try {
            var token = session.getAttribute("access_token").toString();
            if (token.isEmpty()) {
                return;
            }
            var roles = clientService.getRolesFromToken(token);
            if (!roles.contains("owner") && !roles.contains("worker") && !roles.contains("user")) {
                return;
            }
            transactionFeign.sellProduct("Bearer " + token, requestDto);
        } catch (FeignException e) {
            log.error("Can't buy product");
        }
    }

    @Override
    public void fixProduct(VaadinSession session, TransactionRequestDto requestDto) {
        try {
            var token = session.getAttribute("access_token").toString();
            if (token.isEmpty()) {
                return;
            }
            var roles = clientService.getRolesFromToken(token);
            if (!roles.contains("owner") && !roles.contains("worker")) {
                return;
            }
            transactionFeign.fixProduct("Bearer " + token, requestDto);
        } catch (FeignException e) {
            log.error("Can't buy product");
        }
    }
}
