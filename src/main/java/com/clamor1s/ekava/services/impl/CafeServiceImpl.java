package com.clamor1s.ekava.services.impl;

import com.clamor1s.ekava.dto.request.CafeRequestDto;
import com.clamor1s.ekava.dto.response.CafeResponseDto;
import com.clamor1s.ekava.feignclient.CafeFeign;
import com.clamor1s.ekava.services.CafeService;
import com.clamor1s.ekava.services.ClientService;
import com.vaadin.flow.server.VaadinSession;
import feign.FeignException;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CafeServiceImpl implements CafeService {

  private final CafeFeign cafeFeign;
  private final ClientService clientService;

  @Override
  public List<CafeResponseDto> getAllCafes(VaadinSession session) {
    try {
      String token = session.getAttribute("access_token").toString();
      if (token.isEmpty()) {
        return null;
      }
      var roles = clientService.getRolesFromToken(token);
      if (!roles.contains("owner")) {
        return null;
      }
      return cafeFeign.getCafes("Bearer " + token);
    } catch (FeignException e) {
      return null;
    }
  }

  @Override
  public CafeResponseDto getCafeById(VaadinSession session, String id) {
    try {
      String token = session.getAttribute("access_token").toString();
      if (token.isEmpty()) {
        return null;
      }
      var roles = clientService.getRolesFromToken(token);
      if (!roles.contains("owner") && !roles.contains("worker")) {
        return null;
      }
      return cafeFeign.getCafeById("Bearer " + token, UUID.fromString(id));
    } catch (FeignException | IllegalArgumentException e) {
      return null;
    }
  }

  @Override
  public void createCafe(VaadinSession session, CafeRequestDto cafeRequestDto) {
    try {
      String token = session.getAttribute("access_token").toString();
      if (token.isEmpty()) {
        session.setAttribute("access_token", null);
      }
      var roles = clientService.getRolesFromToken(token);
      if (!roles.contains("owner")) {
        return;
      }
      cafeFeign.createCafe("Bearer " + token, cafeRequestDto);
    } catch (FeignException e) {
      log.error("Cant create cafe");
    }
  }
}
