package com.clamor1s.ekava.services;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.clamor1s.ekava.dto.request.CreateUserRequest;
import com.clamor1s.ekava.dto.request.LoginUserRequest;
import com.clamor1s.ekava.dto.response.ClientResponse;
import com.vaadin.flow.server.VaadinSession;

public interface ClientService {
  boolean registerUser(CreateUserRequest userRequest);

  boolean login(VaadinSession session, LoginUserRequest userRequest);

  DecodedJWT validateToken(String token);

  ClientResponse getClient(String bearerToken);

  String getRolesFromToken(String bearerToken);
}
