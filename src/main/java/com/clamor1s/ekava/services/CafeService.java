package com.clamor1s.ekava.services;

import com.clamor1s.ekava.dto.request.CafeRequestDto;
import com.clamor1s.ekava.dto.response.CafeResponseDto;
import com.vaadin.flow.server.VaadinSession;
import java.util.List;

public interface CafeService {
  List<CafeResponseDto> getAllCafes(VaadinSession session);

  CafeResponseDto getCafeById(VaadinSession session, String id);

  void createCafe(VaadinSession session, CafeRequestDto cafeRequestDto);
}
