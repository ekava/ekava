package com.clamor1s.ekava.services;

import com.vaadin.flow.server.VaadinSession;

import java.math.BigDecimal;

public interface StatisticService {
    BigDecimal getProfit(VaadinSession session);
}
