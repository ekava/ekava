package com.clamor1s.ekava.enums;

public enum TransactionType {
    BUY,
    SELL,
    FIX
}
