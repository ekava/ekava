package com.clamor1s.ekava.enums;

public enum TransactionStatus {
    ACCEPTED,
    DECLINED,
    PROCESSING
}
