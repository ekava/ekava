package com.clamor1s.ekava.util.jwt;

import com.auth0.jwt.interfaces.DecodedJWT;

public interface JwtValidator {
  DecodedJWT validate(String token) throws IllegalAccessException;
}
