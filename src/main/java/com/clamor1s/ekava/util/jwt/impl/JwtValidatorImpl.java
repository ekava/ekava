package com.clamor1s.ekava.util.jwt.impl;


import com.auth0.jwk.JwkException;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.UrlJwkProvider;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.clamor1s.ekava.util.jwt.JwtValidator;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.interfaces.RSAPublicKey;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class JwtValidatorImpl implements JwtValidator {

  private String getKeycloakCertificateUrl(DecodedJWT token) {
    return token.getIssuer() + "/protocol/openid-connect/certs";
  }

  private RSAPublicKey loadPublicKey(DecodedJWT token) throws JwkException, MalformedURLException {

    final String url = getKeycloakCertificateUrl(token);
    JwkProvider provider = new UrlJwkProvider(new URL(url));

    return (RSAPublicKey) provider.get(token.getKeyId()).getPublicKey();
  }

  @Override
  public DecodedJWT validate(String token) {
    try {
      final DecodedJWT jwt = JWT.decode(token);

      RSAPublicKey publicKey = loadPublicKey(jwt);

      Algorithm algorithm = Algorithm.RSA256(publicKey, null);
      JWTVerifier verifier = JWT.require(algorithm)
          .withIssuer(jwt.getIssuer())
          .build();

      verifier.verify(token);
      return jwt;

    } catch (Exception e) {
      log.error("Failed to validate JWT", e);
      return null;
    }
  }
}
