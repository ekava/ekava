package com.clamor1s.ekava.dto.response;

import java.math.BigDecimal;
import java.util.UUID;

public record ItemResponseDto(
    UUID id,
    String name,
    String description,
    BigDecimal buyPrice) {
  @Override
  public String toString() {
    return """
        Id: %s |
        Name: %s |
        Description: %s |
        buyPrice: %s
        """.formatted(id, name, description, buyPrice);
  }
}