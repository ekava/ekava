package com.clamor1s.ekava.dto.response;

import java.util.UUID;

public record CafeResponseDto(
    UUID id,
    String name,
    String city,
    String address) {
  @Override
  public String toString() {
    return """
        Id: %s |
        Name: %s |
        City: %s |
        Address: %s
        """.formatted(id, name, city, address);
  }
}