package com.clamor1s.ekava.dto.response;

import java.util.UUID;

public record KeycloakUserRoleResponse(
    UUID id,
    String name,
    String description,
    Boolean composite,
    Boolean clientRole,
    UUID containerId
) {
}
