package com.clamor1s.ekava.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public record KeycloakTokenResponse(
    String access_token,

    @JsonProperty("expires_in")
    Long expiresIn,

    Long refresh_expires_in,

    @JsonProperty("refresh_token")
    String refreshToken,

    @JsonProperty("token_type")
    String tokenType,

    Long before,

    @JsonProperty("session_state")
    String sessionState,

    String scope
) {
}
