package com.clamor1s.ekava.dto.response;

import com.clamor1s.ekava.enums.TransactionStatus;
import com.clamor1s.ekava.enums.TransactionType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public record TransactionResponseDto(
        UUID id,
        LocalDateTime date,
        UUID clientId,
        UUID itemId,
        BigDecimal price,
        TransactionType type,
        TransactionStatus status) implements Serializable {
}