package com.clamor1s.ekava.dto.response;

import java.util.UUID;

public record ClientResponse(
    UUID id,
    String name,
    String surname,
    String email) {
}
