package com.clamor1s.ekava.dto.request;

import java.math.BigDecimal;

public record ItemRequestDto(
    String name,
    String description,
    BigDecimal buyPrice) {
}