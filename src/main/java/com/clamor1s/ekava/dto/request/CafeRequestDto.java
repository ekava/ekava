package com.clamor1s.ekava.dto.request;

import jakarta.validation.constraints.NotNull;

public record CafeRequestDto(
    @NotNull String name,
    @NotNull String city,
    @NotNull String address) {
}