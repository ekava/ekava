package com.clamor1s.ekava.dto.request;

import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.util.UUID;

public record TransactionRequestDto(
        @NotNull UUID itemId,
        @NotNull BigDecimal price) {
}