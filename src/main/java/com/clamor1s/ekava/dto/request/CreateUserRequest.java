package com.clamor1s.ekava.dto.request;

public record CreateUserRequest(
    String name,
    String surname,
    String email,
    String password
) {
}
