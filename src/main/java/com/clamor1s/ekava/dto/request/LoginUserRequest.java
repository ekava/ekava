package com.clamor1s.ekava.dto.request;

public record LoginUserRequest(
    String username,
    String password
) {
}
